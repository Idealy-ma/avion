/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.model;

import com.example.demo.dbmanager.annotation.PrimaryKey;
import com.example.demo.dbmanager.bdd.object.BddObject;
import com.example.demo.dbmanager.connection.BDD;

import java.util.Date;

/**
 *
 * @author Cedrick
 */
public class Entretien extends BddObject {
   @PrimaryKey
    private int idEntretien;
    private Avion avion;
    private int idAvion;
    private Date dateentretien;
    private int etat;

    public int getIdEntretien() {
        return idEntretien;
    }

    public void setIdEntretien(int idEntretien) {
        this.idEntretien = idEntretien;
    }

    public Avion getAvion() throws Exception {
        try {
            if( this.avion == null ){
                this.avion = new Avion();
                this.avion.setIdAvion(this.getIdAvion());
                BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
                this.avion.find(bdd.getConnection());
                bdd.getConnection().close();
            }
        } catch (Exception e) {
            throw e;
        }
        return avion;
    }

    public void setAvion(Avion avion) {
        this.avion = avion;
    }

    public int getIdAvion() {
        return idAvion;
    }

    public void setIdAvion(int idAvion) {
        this.idAvion = idAvion;
    }

    public Date getDateentretien() {
        return dateentretien;
    }

    public void setDateentretien(Date dateentretien) {
        this.dateentretien = dateentretien;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    
}
