package com.example.demo.model;

import com.example.demo.dbmanager.annotation.PrimaryKey;
import com.example.demo.dbmanager.bdd.object.BddObject;

public class Avion extends BddObject {
    @PrimaryKey
    private int idAvion;
    private String numeroAvion;
    private String photoAvion;
    
    public Avion() {
    }

    public Avion(int idAvion, String numeroAvion) {
        this.idAvion = idAvion;
        this.numeroAvion = numeroAvion;
    }

    public Avion(int idAvion, String numeroAvion, String photoAvion) {
        this.idAvion = idAvion;
        this.numeroAvion = numeroAvion;
        this.photoAvion = photoAvion;
    }

    public int getIdAvion() {
        return idAvion;
    }

    public void setIdAvion(int idAvion) {
        this.idAvion = idAvion;
    }

    public String getNumeroAvion() {
        return numeroAvion;
    }

    public void setNumeroAvion(String numeroAvion) {
        this.numeroAvion = numeroAvion;
    }

    public String getPhotoAvion() {
        return photoAvion;
    }

    public void setPhotoAvion(String photoAvion) {
        this.photoAvion = photoAvion;
    }


}