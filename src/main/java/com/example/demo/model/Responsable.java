package com.example.demo.model;

import com.example.demo.util.security.TokenUserModel;
import com.example.demo.dbmanager.annotation.PrimaryKey;
import com.example.demo.dbmanager.bdd.object.BddObject;
import com.example.demo.dbmanager.connection.BDD;
import com.example.demo.util.security.Security;

import java.sql.Connection;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Responsable extends BddObject{
    @PrimaryKey
    private int id;
    private String email;
    private String motDePasse;
    private TokenUserModel myToken;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getMotDePasse() {
        return motDePasse;
    }
    
    public void generateToken() throws Exception{
        TokenUserModel tum = new TokenUserModel();
        tum.setUserId(this.getId());
        tum.setHash(Security.getMd5(String.valueOf(this.getId())));
        tum.setExpirationDate(Timestamp.valueOf(LocalDateTime.now()));
        try {
            BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
            Connection c = bdd.getConnection();
            tum.find(c);
            c.close();
            this.setMyToken(tum);
        } catch (Exception ex) {
            Logger.getLogger(Responsable.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }

    public TokenUserModel getMyToken() {
        if(this.myToken == null){
            try {
                this.myToken = new TokenUserModel();
                myToken.setUserId(this.getId());
                BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
                Connection c = bdd.getConnection();
                myToken.find(c);
                c.close();
                // TODO : verification si le token est expirer
                if(myToken.getHash()==null) this.generateToken();
            } catch (Exception ex) {
                Logger.getLogger(Responsable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return myToken;
    }

    public void setMyToken(TokenUserModel myToken) {
        this.myToken = myToken;
    }
}