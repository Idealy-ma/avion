package com.example.demo.model;

import com.example.demo.dbmanager.annotation.PrimaryKey;
import com.example.demo.dbmanager.bdd.object.BddObject;
import com.example.demo.dbmanager.connection.BDD;

import java.util.Date;

public class Kilometrage extends BddObject{
    @PrimaryKey
    private int idKilometrage;
    private Avion avion;
    private int idAvion;
    private Date dateKm;
    private Double debutKm;
    private Double finKm;


    public int getIdAvion() {
        return idAvion;
    }

    public void setIdAvion(int idAvion) {
        this.idAvion = idAvion;
    }

    
    public void setIdKilometrage(int idKilometrage) {
        this.idKilometrage = idKilometrage;
    }

    public int getIdKilometrage() {
        return idKilometrage;
    }

    public void setAvion(Avion avion) {
        this.avion = avion;
    }

    public Avion getAvion() throws Exception {
        try {
            if( this.avion == null ){
                this.avion = new Avion();
                this.avion.setIdAvion(this.getIdAvion());
                BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
                this.avion.find(bdd.getConnection());
                bdd.getConnection().close();
            }
        } catch (Exception e) {
            throw e;
        }
        return avion;
    }

    public void setDateKm(Date dateKm) {
        this.dateKm = dateKm;
    }

    public Date getDateKm() {
        return dateKm;
    }

    public void setDebutKm(Double debutKm) {
        this.debutKm = debutKm;
    }

    public Double getDebutKm() {
        return debutKm;
    }

    public void setFinKm(Double finKm) {
        this.finKm = finKm;
    }

    public Double getFinKm() {
        return finKm;
    }
}