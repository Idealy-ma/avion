/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

import com.example.demo.dbmanager.bdd.object.BddObject;
import com.example.demo.dbmanager.connection.BDD;

/**
 *
 * @author P14A_30_Ando
 */
public class Assurance extends BddObject{
    private int idAssurance;
    private Avion avion;
    private int idAvion;
    private Date dateDebut;
    private Date dateFin;
    String assureur;

    public Assurance() {
    }

    public Assurance(int idAssurance, Avion avion, int idAvion, Date dateDebut, Date dateFin, String assureur) {
        this.idAssurance = idAssurance;
        this.avion = avion;
        this.idAvion = idAvion;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.assureur = assureur;
    }

    public int getIdAssurance() {
        return idAssurance;
    }

    public void setIdAssurance(int idAssurance) {
        this.idAssurance = idAssurance;
    }

    public Avion getAvion() throws Exception {
        try {
            if( this.avion == null ){
                this.avion = new Avion();
                this.avion.setIdAvion(this.getIdAvion());
                BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
                this.avion.find(bdd.getConnection());
                bdd.getConnection().close();
            }
        } catch (Exception e) {
            throw e;
        }
        return avion;
    }

    public void setAvion(Avion avion) {
        this.avion = avion;
    }

    public int getIdAvion() {
        return idAvion;
    }

    public void setIdAvion(int idAvion) {
        this.idAvion = idAvion;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public String getAssureur() {
        return assureur;
    }

    public void setAssureur(String assureur) {
        this.assureur = assureur;
    }
    
    public ArrayList<Object> assuranceExpirer(int mois) throws Exception {
        ArrayList<Object> arrayList = new ArrayList<>();
        LocalDate localDate = LocalDate.now();
        LocalDate dateLimite = localDate.plusMonths(mois);
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
            connection = bdd.getConnection();
            System.out.println("SELECT * FROM assurance WHERE datefin < '"+dateLimite+"'");
            statement = connection.prepareStatement("select * from assurance where datefin < '"+dateLimite+"'");
            result = statement.executeQuery();
            while(result.next()){
                Assurance assurance = new Assurance();
                assurance.setIdAssurance(result.getInt("idAssurance"));
                assurance.setIdAvion(result.getInt("idAvion"));
                assurance.setDateDebut(result.getDate("DateDebut"));
                assurance.setDateFin(result.getDate("DateFin"));
                arrayList.add(assurance);
            }
        } catch (Exception e) {
            // TODO: handle exception
            throw e;
        } finally {
            if(result != null){
                result.close();
            }
            if(statement != null){
                statement.close();
            }
            if(connection != null){
                connection.close();
            }
        }
        return arrayList;
    }
}
