package com.example.demo.controller;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dbmanager.connection.BDD;
import com.example.demo.model.Assurance;
import com.example.demo.util.exception.JSONException;


@RestController
@CrossOrigin
public class AssuranceController {
    HashMap<String, Object> returnValue;
    
    public AssuranceController() {
        returnValue = new HashMap<>();
    }

    public HashMap<String, Object> getReturnValue() {
        return returnValue;
    }

    public void setReturnValue(HashMap<String, Object> returnValue) {
        this.returnValue = returnValue;
    }

    @PostMapping("/assurances")
    public HashMap<String, Object> addAssurance(@RequestHeader(name="idAvion") int idAvion,@RequestHeader(name="dateDebut") Date dateDebut,@RequestHeader(name="dateFin") Date dateFin,@RequestHeader(name="assureur") String assureur) throws Exception{
        try {
            returnValue.clear();
            BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
            Connection c = bdd.getConnection();
            Assurance assurance = new Assurance();
            assurance.setIdAvion(idAvion);
            assurance.setDateDebut(dateDebut);
            assurance.setDateFin(dateFin);
            assurance.setAssureur(assureur);
            assurance.create(c);
            
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(AssuranceController.class.getName()).log(Level.SEVERE, null, ex);
            returnValue.put("error", new JSONException("500", ex.getMessage()));
            return returnValue; 
        }
        returnValue.put("response", new JSONException("200", "Insertion OK"));
        return returnValue;
    }
    
    @GetMapping("/assurances")
    public HashMap<String, Object> findAssurance() throws Exception{
        returnValue.clear();
        ArrayList<Assurance> listeAssurance = new ArrayList<>();
        try {
            BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
            Connection c = bdd.getConnection();
            Assurance assurance = new Assurance();
            ArrayList<Object> listeObjectAssurance = assurance.findAll(c);
            
            
            for (Object assur : listeObjectAssurance) {
                listeAssurance.add((Assurance)assur);
            }
            
            if (!listeAssurance.isEmpty()) {
                returnValue.put("data", listeAssurance);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(AssuranceController.class.getName()).log(Level.SEVERE, null, ex);
            returnValue.put("error", new JSONException("500", ex.getMessage()));
            return returnValue; 
        }
        return returnValue;
    }

    @GetMapping("/assurances/{idAvion}")
    public HashMap<String, Object> findAssuranceByIdAvion(@PathVariable int idAvion) throws Exception{
        returnValue.clear();
        ArrayList<Assurance> listeAssurance = new ArrayList<>();
        try {
            BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
            Connection c = bdd.getConnection();
            Assurance assurance = new Assurance();
            assurance.setIdAvion(idAvion);
            ArrayList<Object> listeObjectAssurance = assurance.findAll(c);
            
            for (Object assur : listeObjectAssurance) {
                listeAssurance.add((Assurance)assur);
            }
            
            if (!listeAssurance.isEmpty()) {
                returnValue.put("data", listeAssurance);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(AssuranceController.class.getName()).log(Level.SEVERE, null, ex);
            returnValue.put("error", new JSONException("500", ex.getMessage()));
            return returnValue; 
        }
        return returnValue;
    }

    @GetMapping("/expiration/{mois}")
    public HashMap<String, Object> getOneAssurance(@PathVariable int mois) throws Exception{
        returnValue.clear();
        ArrayList<Assurance> listeAssurance = new ArrayList<>();
        try {
            Assurance assurance = new Assurance();
            ArrayList<Object> listeObjectAssurance = assurance.assuranceExpirer(mois);
            
            for (Object assur : listeObjectAssurance) {
                listeAssurance.add((Assurance)assur);
            }
            
            if (!listeAssurance.isEmpty()) {
                returnValue.put("data", listeAssurance);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(AssuranceController.class.getName()).log(Level.SEVERE, null, ex);
            returnValue.put("error", new JSONException("500", ex.getMessage()));
            return returnValue; 
        }
        return returnValue;
    }
}
