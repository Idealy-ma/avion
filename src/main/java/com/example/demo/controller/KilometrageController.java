package com.example.demo.controller;

import com.example.demo.dbmanager.connection.BDD;
import com.example.demo.model.Kilometrage;
import com.example.demo.util.exception.JSONException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class KilometrageController{
    HashMap<String, Object> returnValue;

    public KilometrageController() {
        returnValue = new HashMap<>();
    }
    
    @PostMapping("/kilometrages")
    public HashMap<String, Object> addKilometrage(@RequestHeader(name="idAvion") int idAvion,@RequestHeader(name="dateKm") Date dateKm,@RequestHeader(name="debutKm") Double debutKm,@RequestHeader(name="finKm") Double finKm) throws Exception{
        try {
            returnValue.clear();
            BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
            Connection c = bdd.getConnection();
            Kilometrage kilometrage = new Kilometrage();

            kilometrage.setIdAvion(idAvion);
            kilometrage.setDateKm(dateKm);
            kilometrage.setDateKm(dateKm);
            kilometrage.setFinKm(finKm);
            kilometrage.create(c);
            
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Kilometrage.class.getName()).log(Level.SEVERE, null, ex);
            returnValue.put("error", new JSONException("500", ex.getMessage()));
            return returnValue; 
        }
        returnValue.put("response", new JSONException("200", "Insertion OK"));
        return returnValue;
    }
    
    @GetMapping("/kilometrages")
    public HashMap<String, Object> findKilometrage() throws Exception{
        returnValue.clear();
        ArrayList<Kilometrage> listeKilometrage = new ArrayList<>();
        try {
            BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
            Connection c = bdd.getConnection();
            Kilometrage kilometrage = new Kilometrage();
            ArrayList<Object> listeObjectKilometrage = kilometrage.findAll(c);
            
            for (Object kilometre : listeObjectKilometrage) {
                listeKilometrage.add((Kilometrage)kilometre);
            }
            
            if (!listeKilometrage.isEmpty()) {
                returnValue.put("data", listeKilometrage);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Kilometrage.class.getName()).log(Level.SEVERE, null, ex);
            returnValue.put("error", new JSONException("500", ex.getMessage()));
            return returnValue; 
        }
        return returnValue;
    }

    @GetMapping("/kilometrages/{idAvion}")
    public HashMap<String, Object> findKilometrageByIdAvion(@PathVariable int idAvion) throws Exception{
        returnValue.clear();
        ArrayList<Kilometrage> listeKilometrage = new ArrayList<>();
        try {
            BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
            Connection c = bdd.getConnection();
            Kilometrage kilometrage = new Kilometrage();
            kilometrage.setIdAvion(idAvion);
            ArrayList<Object> listeObjectKilometrage = kilometrage.findAll(c);
            
            for (Object kilometre : listeObjectKilometrage) {
                listeKilometrage.add((Kilometrage)kilometre);
            }
            
            if (!listeKilometrage.isEmpty()) {
                returnValue.put("data", listeKilometrage);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Kilometrage.class.getName()).log(Level.SEVERE, null, ex);
            returnValue.put("error", new JSONException("500", ex.getMessage()));
            return returnValue; 
        }
        return returnValue;
    }
}
