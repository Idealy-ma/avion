package com.example.demo.controller;

import com.example.demo.dbmanager.connection.BDD;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import com.example.demo.model.Avion;
import com.example.demo.util.exception.JSONException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


@RestController
@CrossOrigin
public class AvionController{
    HashMap<String, Object> returnValue;

    public AvionController() {
        returnValue = new HashMap<>();
    }
    
    @PostMapping("/avions")
    public HashMap<String, Object> addAvion(@RequestHeader(name="matricule") String matricule,@RequestHeader(name="photo") String photo) throws Exception{
        try {
            returnValue.clear();
            BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
            Connection c = bdd.getConnection();
            Avion v = new Avion();

            v.setNumeroAvion(matricule);
            v.setPhotoAvion(photo);
            v.create(c);
            
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(AvionController.class.getName()).log(Level.SEVERE, null, ex);
            returnValue.put("error", new JSONException("500", ex.getMessage()));
            return returnValue; 
        }
        returnValue.put("response", new JSONException("200", "Insertion OK"));
        return returnValue;
    }
    
    @GetMapping("/avions")
    public HashMap<String, Object> findAvion() throws Exception{
        returnValue.clear();
        ArrayList<Avion> listeAvion = new ArrayList<>();
        try {
            BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
            Connection c = bdd.getConnection();
            Avion v = new Avion();
            ArrayList<Object> listeObjectAvion = v.findAll(c);
            
            
            for (Object avion : listeObjectAvion) {
                listeAvion.add((Avion)avion);
            }
            
            if (!listeAvion.isEmpty()) {
                returnValue.put("data", listeAvion);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(AvionController.class.getName()).log(Level.SEVERE, null, ex);
            returnValue.put("error", new JSONException("500", ex.getMessage()));
            return returnValue; 
        }
        return returnValue;
    }

    @GetMapping("/avions/{idAvion}")
    public HashMap<String, Object> findAvionById(@PathVariable int idAvion) throws Exception{
        returnValue.clear();
        ArrayList<Avion> listeAvion = new ArrayList<>();
        try {
            BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
            Connection c = bdd.getConnection();
            Avion v = new Avion();
            v.setIdAvion(idAvion);
            ArrayList<Object> listeObjectAvion = v.findAll(c);
            
            
            for (Object avion : listeObjectAvion) {
                listeAvion.add((Avion)avion);
            }
            
            if (!listeAvion.isEmpty()) {
                returnValue.put("data", listeAvion);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(AvionController.class.getName()).log(Level.SEVERE, null, ex);
            returnValue.put("error", new JSONException("500", ex.getMessage()));
            return returnValue; 
        }
        return returnValue;
    }
}