package com.example.demo.controller;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dbmanager.connection.BDD;
import com.example.demo.model.Entretien;
import com.example.demo.util.exception.JSONException;

@RestController
@CrossOrigin
public class EntretienController {
    HashMap<String, Object> returnValue;

    public HashMap<String, Object> getReturnValue() {
        return returnValue;
    }

    public void setReturnValue(HashMap<String, Object> returnValue) {
        this.returnValue = returnValue;
    }

    public EntretienController() {
        returnValue = new HashMap<>();
    }    

    @PostMapping("/entretiens")
    public HashMap<String, Object> addEntretien(@RequestHeader(name="idAvion") int idAvion,@RequestHeader(name="dateEntretien") Date dateEntretien,@RequestHeader(name="etat") int etat) throws Exception{
        try {
            returnValue.clear();
            BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
            Connection c = bdd.getConnection();
            Entretien entretien = new Entretien();
            entretien.setIdAvion(idAvion);
            entretien.setDateentretien(dateEntretien);
            entretien.setEtat(etat);
            entretien.create(c);
            
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(EntretienController.class.getName()).log(Level.SEVERE, null, ex);
            returnValue.put("error", new JSONException("500", ex.getMessage()));
            return returnValue; 
        }
        returnValue.put("response", new JSONException("200", "Insertion OK"));
        return returnValue;
    }
    @GetMapping("/entretiens")
    public HashMap<String, Object> findEntretien() throws Exception{
        returnValue.clear();
        ArrayList<Entretien> listeEntretien = new ArrayList<>();
        try {
            BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
            Connection c = bdd.getConnection();
            Entretien entretien = new Entretien();
            ArrayList<Object> listeObjectEntretien = entretien.findAll(c);
            
            for (Object assur : listeObjectEntretien) {
                listeEntretien.add((Entretien)assur);
            }
            
            if (!listeEntretien.isEmpty()) {
                returnValue.put("data", listeEntretien);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(EntretienController.class.getName()).log(Level.SEVERE, null, ex);
            returnValue.put("error", new JSONException("500", ex.getMessage()));
            return returnValue; 
        }
        return returnValue;
    }

    @GetMapping("/entretiens/{idAvion}")
    public HashMap<String, Object> findEntretienByIdAvion(@PathVariable int idAvion) throws Exception{
        returnValue.clear();
        ArrayList<Entretien> listeEntretien = new ArrayList<>();
        try {
            BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
            Connection c = bdd.getConnection();
            Entretien entretien = new Entretien();
            entretien.setIdAvion(idAvion);
            ArrayList<Object> listeObjectEntretien = entretien.findAll(c);
            
            for (Object assur : listeObjectEntretien) {
                listeEntretien.add((Entretien)assur);
            }
            
            if (!listeEntretien.isEmpty()) {
                returnValue.put("data", listeEntretien);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(AssuranceController.class.getName()).log(Level.SEVERE, null, ex);
            returnValue.put("error", new JSONException("500", ex.getMessage()));
            return returnValue; 
        }
        return returnValue;
    }
}
