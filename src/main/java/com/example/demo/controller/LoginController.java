package com.example.demo.controller;

import com.example.demo.dbmanager.connection.BDD;
import com.example.demo.model.Responsable;
import com.example.demo.util.exception.JSONException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class LoginController{
    HashMap<String, Object> returnValue;
    
    public LoginController() {
        returnValue = new HashMap<>();
    }
    
    @PostMapping("/responsables")
    public HashMap<String, Object> login(@RequestHeader(name="email") String email, @RequestHeader(name="motdepasse") String mdp) throws Exception{
        try {
            returnValue.clear();
            BDD bdd = new BDD("postgres", "sFROPOwowro1LNFQLGXB", "railway");
            try (Connection c = bdd.getConnection()) {
                Responsable resp = new Responsable();
                resp.setEmail(email);
                resp.setMotDePasse(mdp);

                resp.find(c);
                if(resp.getId()>0){
                    returnValue.put("data", resp.getMyToken());
                } else {
                    returnValue.put("error", new JSONException("403", "User not found"));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            returnValue.put("error", new JSONException("500", ex.getMessage()));
            return returnValue;
        }
        returnValue.put("response", new JSONException("200", "Insertion OK"));
        return returnValue;
    }

    public HashMap<String, Object> getReturnValue() {
        return returnValue;
    }

    public void setReturnValue(HashMap<String, Object> returnValue) {
        this.returnValue = returnValue;
    }
}