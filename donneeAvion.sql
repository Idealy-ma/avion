INSERT INTO
    avion(numeroAvion,photoAvion)
VALUES
    ('Boing727','Boing727.jpg'),
    ('Boing747','boing747.jpg'),
    ('AirPlane','AirPlane.jpg'),
    ('V-17','V-17.jpg'),
    ('V-18','V-18.jpg');

INSERT INTO responsable(email,motdepasse) VALUES('responsable@gmail.com','responsable');

INSERT INTO
    assurance(idAvion,dateDebut,dateFin,assureur) 
VALUES
    (1,'2022-12-01','2023-01-01','ARO'),
    (2,'2022-12-01','2023-01-01','ARO'),
    (3,'2022-12-01','2023-01-01','ARO'),
    (4,'2022-12-01','2023-03-01','HAVANA'),
    (5,'2022-12-01','2023-03-01','HAVANA');

INSERT INTO
    kilometrage(idAvion,debutKm,finKm,dateKm)
VALUES
    (1,'11107','15209','2022-12-20'),
    (2,'13501','17621','2022-12-20'),
    (3,'10002','14320','2022-12-20'),
    (4,'12245','30268','2022-12-20'),
    (5,'14264','29354','2022-12-20');

INSERT INTO
    entretien(idAvion,dateEntretien)
VALUES
    (1,'2022-12-31'),
    (2,'2022-12-31'),
    (3,'2022-12-31'),
    (4,'2022-12-31'),
    (5,'2022-12-31');