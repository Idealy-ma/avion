CREATE TABLE avion(
    idAvion serial PRIMARY KEY,
    numeroAvion VARCHAR(20) UNIQUE,
    photoAvion VARCHAR(50)
);

CREATE TABLE responsable(
    id serial PRIMARY KEY,
    email varchar(50) UNIQUE,
    motdepasse varchar(30)
);

CREATE TABLE assurance(
    idAssurance serial PRIMARY KEY,
    idAvion int REFERENCES avion(idAvion),
    dateDebut date DEFAULT now(),
    dateFin date,
    assureur VARCHAR(20)
);

CREATE TABLE kilometrage(
    idKilometrage serial PRIMARY KEY,
    idAvion int references avion(idAvion),
    debutKm double precision DEFAULT 0,
    finKm double precision DEFAULT 0,
    dateKm date DEFAULT now()
);
 
CREATE TABLE entretien(
    idEntretien serial PRIMARY KEY,
    idAvion int references avion(idAvion),
    dateEntretien date DEFAULT now(),
    etat int DEFAULT 1
);

CREATE TABLE tokenUserModel(
    userId int,
    hash VARCHAR(255),
    expirationDate Timestamp DEFAULT CURRENT_TIMESTAMP
);
